#include<iostream>
#include<fstream>
#include<string>
#include<stdlib.h>
#include"scanner.cpp"
using namespace std;

// INSTRUCTION:  Complete all ** parts.
// You may use any method to connect this file to scanner.cpp
// that you had written.  
// You can copy scanner.cpp here by cp ../ScannerFiles/scanner.cpp . and do 
// cat scanner.cpp parser.cpp > myparser.cpp
// -----------------------------------------------------

//=================================================
// File parser.cpp written by Group Number: *5*
//=================================================

// ----- Utility and Globals -----------------------------------

// ** Need syntaxerror1 and syntaxerror2 functions (each takes 2 args)
// ** Be sure to put the name of the programmer above each function
// i.e. Done by:Mike Yoon
void s();
void afterSubject();
void afterNoun();
void afterObject();
void noun();
void verb();
void be();
void tense();

string saved_lexeme;
bool token_available = false;
tokentype saved_token;

//Done by: Mike Yoon
void syntax_error1(tokentype expected, string saved_lexeme){
  cout<<"Syntax error: expected "<<tokenName[expected] << " but instead: "<< saved_lexeme << endl;
  exit(1);
}
//Done by:Mike Yoon
void syntax_error2(string saved_lexeme, string parserFunct){
  cout<<"Syntax error: unexpected "<< saved_lexeme<<" found in " << parserFunct << endl;
  exit(1);
}

// ** Need the updated match and next_token (with 2 global vars)
// ** Be sure to put the name of the programmer above each function
// i.e. Done by:Mike Yoon
tokentype next_token(){
  if(!token_available){
    scanner(saved_token, saved_lexeme);
    cout<<"Scanner called using: "<< saved_lexeme << endl;
    token_available = true;
  }
  return saved_token;
}
//Done by :Mike Yoon
bool match(tokentype expected){
  if(next_token() != expected){
    syntax_error1(expected, saved_lexeme);
  }else{
    cout<<"Matched"<<tokenName[expected]<<endl;
    token_available = false;
    return true;
  }
}
// ----- RDP functions - one per non-term -------------------

// ** Make each non-terminal into a function here
// ** Be sure to put the corresponding grammar rule above each function
// i.e. Grammar: 
// ** Be sure to put the name of the programmer above each function
// i.e. Done by:GenHong Lin
void story(){
  cout<<"Processing <story>"<<endl;
  s();
  while(true && (saved_lexeme != "eofm")){
    s();
  }
  cout<<"\nSuccess"<<endl;
}
//Done by: GenHong Lin
void s(){
  next_token();
  if(saved_lexeme != "eofm"){
    cout<<"Processing <s>" << endl;
    if(next_token() == CONNECTOR){
      match(CONNECTOR);
    }
    noun();
    match(SUBJECT);
    afterSubject();
  }
}

//<noun>::= WORD1|PRONOUN
//Done by:GenHong Lin
void noun(){
  cout<<"Processing <noun>"<<endl;
  switch(next_token()){
  case WORD1:
    match(WORD1);
    break;
  case PRONOUN:
    match(PRONOUN);
    break;
  default:syntax_error2(saved_lexeme, "noun");
  }
}

//<after subject>::=<verb><tense> PERIOD | <noun><after noun>
//Done by:GenHong Lin
void afterSubject(){
  cout<<"Processing <afterSubject>"<<endl;
  switch(next_token()){
  case WORD2:
    verb();
    tense();
    match(PERIOD);
    break;
  case WORD1:
    noun();
    afterNoun();
    break;
  case PRONOUN:
    noun();
    afterNoun();
    break;
  default:syntax_error2(saved_lexeme, "afterSubject");
  }
}

//<verb>::= WORD2
//Done by:GenHong Lin
void verb(){
  cout<<"Processing <verb>"<<endl;
  switch(next_token()){
  case WORD2:
    match(WORD2);
    break;
  default:syntax_error2(saved_lexeme, "verb");
  }
}

//<be>::= IS | WAS
//Done by:GenHong Lin
void be(){
  cout<<"Procesing <be>"<<endl;
  switch(next_token()){
  case IS:
    match(IS);
    break;
  case WAS:
    match(WAS);
    break;
  default:syntax_error2(saved_lexeme, "be");
  }
}

//<after noun>::=<be> PERIOD | DESTINATION <verb> <tense> PERIOD | OBJECT <after object>
//Done by:GenHong Lin
void afterNoun(){
  cout<<"Processing <afterNoun>"<<endl;
  switch(next_token()){
  case IS:
    be();
    match(PERIOD);
    break;
  case WAS:
    be();
    match(PERIOD);
    break;
  case DESTINATION:
    match(DESTINATION);
    verb();
    tense();
    match(PERIOD);
    break;
  case OBJECT:
    match(OBJECT);
    afterObject();
    break;
  default:syntax_error2(saved_lexeme, "afterNoun");
  }
}

//<after object>::= <verb><tense> PERIOD | <noun>DESTINATION <verb><tense> PERIOD
//Done by:Ahoura Niknam
void afterObject(){
  cout<<"Processing <afterObject>"<<endl;
  switch(next_token()){
  case WORD2:
    verb();
    tense();
    match(PERIOD);
    break;
  case WORD1:
    noun();
    match(DESTINATION);
    verb();
    tense();
    match(PERIOD);
    break;
  case PRONOUN:
    noun();
    match(DESTINATION);
    verb();
    tense();
    match(PERIOD);
    break;
  default:syntax_error2(saved_lexeme, "afterObject");
  }
}

//<tense> ::= VERBPAST | VERBPASTNEG | VERB | VERBNEG
//Done by: Ahoura Niknam
void tense(){
  cout<<"Processing <tense>"<<endl;
  switch(next_token()){
  case VERBPAST:
    match(VERBPAST);
    break;
  case VERBPASTNEG:
    match(VERBPASTNEG);
    break;
  case VERB:
    match(VERB);
    break;
  case VERBNEG:
    match(VERBNEG);
    break;
  default:syntax_error2(saved_lexeme, "tense");
  }
}
//---------------------------------------

// The new test driver to start the parser
// Done by:  *Ahoura Niknam*
int main()
{
  string filename;
  cout << "Enter the input file name: ";
  cin >> filename;
  fin.open(filename.c_str());

  //** calls the <story> to start parsing
  story();
  //** closes the input file
  fin.close();

}// end
//** require no other input files!
//** syntax error EC requires producing errors.text of messages

