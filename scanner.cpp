#include<iostream>
#include<fstream>
#include<string>
using namespace std;

//=====================================================
// File scanner.cpp written by: Group Number: *5* 
//=====================================================

// --------- DFAs ---------------------------------

// ** MYTOKEN DFA to be replaced by the WORD DFA
// ** Done by:GenHong Lin
// ** RE:
bool word (string s)
{
  int state = 0;
  int charpos = 0;
  
  while (s[charpos] != '\0') //state q0
    {
      if (state == 0 && (s[charpos] == 'b' || s[charpos] == 'g' || s[charpos] == 'h' || s[charpos] == 'k' || s[charpos] == 'm' || s[charpos] == 'n' || s[charpos] == 'p' || s[charpos] == 'r'))
	{state = 1;}//go to q1 if bghkmnpr
      else if(state == 0 && (s[charpos] == 'a' || s[charpos] == 'i' || s[charpos] == 'u' || s[charpos] == 'e' || s[charpos] == 'o' || s[charpos] == 'I' || s[charpos] == 'E'))
	{state = 2;}//go to q2 it vowel
      else if (state == 0 && (s[charpos] == 'd' || s[charpos] == 'j' || s[charpos] == 'w' || s[charpos] == 'y' || s[charpos] == 'z'))
	{state = 3;}//go to q3 if djwyz
      else if(state == 0 && s[charpos] == 's')
	{state = 4;}//go to q4 if s
      else if(state == 0 && s[charpos] == 'c')
	{state = 5;}//go to q5 if c
      else if(state == 0 && s[charpos] == 't')
	{state = 6;}//go to q6 if t
      else{
	if(state == 1 && (s[charpos] == 'a' || s[charpos] == 'i' || s[charpos] == 'u' || s[charpos] == 'e' || s[charpos] == 'o' || s[charpos] == 'I' || s[charpos] == 'E'))
	  {state = 2;}//from q1 to q2 if vowel
	else if(state == 1 && s[charpos] == 'y')
	  {state = 3;}//from q1 to q3 if y
	else{
	  if(state == 2 && s[charpos] == 'n')
	    {state = 0;}//from q2 to q0 if n
	  else if(state == 2 && (s[charpos] == 'b' || s[charpos]  == 'g' || s[charpos] == 'h' || s[charpos] == 'k' || s[charpos] == 'm' || s[charpos] == 'n' || s[charpos] == 'p' || s[charpos] =='r'))
	    {state = 1;}//from q2 to q1 if bghkmnpr
	  else if(state == 2 && (s[charpos] == 'a' || s[charpos] == 'i' || s[charpos] == 'u' || s[charpos] == 'e' || s[charpos] == 'o' || s[charpos] == 'I' || s[charpos] == 'E'))
	    {state = 2;}//from q2 to q2 if vowel
	  else if(state == 2 && (s[charpos] == 'd' || s[charpos] == 'j' || s[charpos] == 'w' || s[charpos] == 'y' || s[charpos] == 'z'))
	    {state = 3;}//from q2 to q3 if djwyz
	  else if(state == 2 && s[charpos] == 's')
	    {state = 4;}//from q2 to q4 if s
	  else if(state == 2 && s[charpos] == 'c')
	    {state = 5;}//from q2 to q5 if c
	  else if(state == 2 &&  s[charpos] == 't')
	    {state = 6;}//from q2 to q6 if t
	  else{
	    if(state == 3 && (s[charpos] == 'a' || s[charpos] == 'i' || s[charpos] == 'u' || s[charpos] == 'e' || s[charpos] == 'o' || s[charpos] == 'I' || s[charpos] == 'E'))
	      {state = 2;}//from q3 to q2 if vowel
	    else{
	      if(state == 4 && (s[charpos] == 'a' || s[charpos] == 'i' || s[charpos] == 'u' || s[charpos] == 'e' || s[charpos] == 'o' || s[charpos] == 'I' || s[charpos] == 'E'))
		{state = 2;}//from q4 to q2 if vowel *not in DFA graph but it should be
	      else if(state == 4 && s[charpos] == 'h')
		{state = 3;}//from q4 to q3 if h
	      else{
		if(state == 5 && s[charpos] == 'h')
		  {state = 3;}//from q5 to q3 if h
		else{
		  if(state == 6 && (s[charpos] == 'a' || s[charpos] == 'i' || s[charpos] == 'u' || s[charpos] == 'e' || s[charpos] == 'o' || s[charpos] == 'I' || s[charpos] == 'E'))
		    {state = 2;}//from q6 to q2 if vowel
		  else if(state == 6 && s[charpos] == 's')
		    {state = 3;}//from q6 to q3 if s
		  else if(state == 6 && s[charpos] == 't')
		    {state = 2;}//from q6 to q2 if t
		  else{
		    return false;
		  }	  
		}
	      }
	    }
	  }
	}
      }
      charpos++;
    }//end of while
  
  // where did I end up????
  if(state == 0 || state == 2)  // end in a final state
    {
      return true;
    }
  return(false);
}
// ** Add the PERIOD DFA here
// ** Done by:GenHong Lin
bool period(string s)
{
  int state = 0;
  int charpos = 0;

  while(s[charpos]  != '\0'){
    if(state == 0 && s[charpos] == '.')
      state = 1;
    else
      return false;
    ++charpos;
  }
  if(state == 1) return true;
  else return(false);
}

  // -----  Tables -------------------------------------
  
  // ** Update the tokentype to be WORD1, WORD2, PERIOD, ERROR, EOFM, etc.
  enum tokentype {ERROR, WORD1, WORD2, PERIOD, VERB, VERBNEG, VERBPAST, VERBPASTNEG, IS, WAS, OBJECT, SUBJECT, DESTINATION, PRONOUN, CONNECTOR};
  
  // ** string tokenName[30] = { }; for the display names oftokens - must be in the same order as the tokentype.
  string tokenName[30] = {"ERROR", "WORD1", "WORD2", "PERIOD", "VERB", "VERBNEG", "VERBPAST", "VERBPASTNEG", "IS", "WAS", "OBJECT", "SUBJECT", "DESTINATION", "PRONOUN", "CONNECTOR"};
  
  struct reserved{
    const char* string;
    tokentype tokenT;
  }
  reserved[] = {
    {"masu", VERB},
    {"masen", VERBNEG},
    {"mashita", VERBPAST},
    {"masendeshita", VERBPASTNEG},
    {"desu", IS},
    {"deshita", WAS},
    {"o", OBJECT},
    {"wa", SUBJECT},
    {"ni", DESTINATION},
    {"watashi", PRONOUN},
    {"anata", PRONOUN},
    {"kare", PRONOUN},
    {"kanojo", PRONOUN},
    {"sore", PRONOUN},
    {"mata", CONNECTOR},
    {"soshite", CONNECTOR},
    {"shikashi", CONNECTOR},
    {"dakara", CONNECTOR},
  };
  
 // ** Need the reservedwords table to be set up here. 
 // ** Do not require any file input for this.
 // ** a.out should work without any additional files.
 

// ------------ Scaner and Driver ----------------------- 

ifstream fin;  // global stream for reading from the input file

// Scanner processes only one word each time it is called
// Gives back the token type and the word itself
// ** Done by:Ahura Niknam, Mike Yoon  
int scanner(tokentype& a, string& w)
{
  
  // ** Grab the next word from the file via fin
  // 1. If it is eofm, return right now.   
  fin >> w;
  /* 
     2. Call the token functions one after another (if-then-else)
     And generate a lexical error message if both DFAs failed.
     Let the token_type be ERROR in that case.
     
     3. Then, make sure WORDs are checked against the reservedwords list
     If not reserved, token_type is WORD1 or WORD2.
     
     4. Return the token type & string  (pass by reference)
     
  */
  int rowCount = sizeof reserved/ sizeof reserved[0];
  if(word(w)){
    for(int i = 0; i < rowCount; i++){
      if(w == reserved[i].string){
	a = reserved[i].tokenT;
	return 0;
      }
    }
    char lastLetter = ' ';
    lastLetter = w[w.length()-1];

    if(lastLetter == 'I' || lastLetter == 'E'){
      a = WORD2;
    }else{
      a = WORD1;
    }
  }
  else if(period(w)){
    a = PERIOD;
  }else if(w == "eofm"){
  }else{
    cout<<"Lexical error: "<<w<<" is not valid\n";
    a = ERROR;
  }
  return 0;
}//the end of scanner



// The temporary test driver to just call the scanner repeatedly  
// This will go away after this assignment
// DO NOT CHANGE THIS!!!!!! 
// Done by:  Rika
/*int main()
{
  tokentype thetype;
  string theword; 
  string filename;

  cout << "Enter the input file name: ";
  cin >> filename;

  fin.open(filename.c_str());

  // the loop continues until eofm is returned.
   while (true)
    {
       scanner(thetype, theword);  // call the scanner
       if (theword == "eofm") break;  // stop now

       cout << "Type is:" << tokenName[thetype] << endl;
       cout << "Word is:" << theword << endl;   
    }

   cout << "End of file is encountered." << endl;
   fin.close();

}// end
*/
